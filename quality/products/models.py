# -*- coding: utf-8 -*-
"""Products app models."""
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models


class Size(models.Model):
    name = models.CharField(max_length=8, verbose_name='nome')
    slug = models.SlugField(verbose_name='slug')

    def __unicode__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=16, verbose_name='nome')
    slug = models.SlugField(verbose_name='slug')
    description = models.TextField(blank=True, verbose_name='descrição')
    sizes = models.ManyToManyField(Size, verbose_name='tamanhos')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('produts:list')
