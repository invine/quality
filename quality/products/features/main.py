# -*- coding: utf-8 -*-
from lettuce import after, before, step, world
from selenium import webdriver


@before.all
def set_browser():
    world.browser = webdriver.Firefox()


@step('dummy stuff')
def dummy_stuff(step):
    world.browser.get("http://0.0.0.0:9000")
    assert 'QA' in world.browser.title


@after.all
def tear_down(total):
    world.browser.close()
