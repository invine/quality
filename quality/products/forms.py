# -*- coding: utf-8 -*-
"""Products app forms."""
from django import forms
from django.utils.text import slugify

from .models import Product


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = (
            'name',
            'description',
            'sizes',
        )

    def save(self, commit=True, *args, **kwargs):
        instance = super(ProductForm, self).save(commit=False, *args, **kwargs)
        name = self.cleaned_data['name']
        instance.slug = slugify(name)
        if commit:
            instance.save()
        return instance
