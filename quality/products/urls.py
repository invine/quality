# -*- coding: utf-8 -*-
"""Products app urls."""
from django.conf.urls import url

from . import views


urlpatterns = [
    url(
        r'^products/add/$',
        views.ProductAdd.as_view(),
        name='add'
    ),
    url(
        r'^products/(?P<slug>[\w-]+)/$',
        views.ProductEdit.as_view(),
        name='edit'
    ),
    url(
        r'^products/(?P<slug>[\w-]+)/delete/$',
        views.ProductDelete.as_view(),
        name='delete'
    ),
    url(
        r'^$',
        views.ProductList.as_view(),
        name='list'
    ),
]
