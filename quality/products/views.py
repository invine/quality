# -*- coding: utf-8 -*-
"""Products app views."""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.views import generic

from .forms import ProductForm
from .models import Product


class ProductMixin(object):
    model = Product


class ProductList(ProductMixin, generic.ListView):
    template_name = 'products/list.html'
    context_object_name = 'products'


class ProductAdd(LoginRequiredMixin, ProductMixin, generic.CreateView):
    template_name = 'products/form.html'
    success_url = reverse_lazy('products:list')
    form_class = ProductForm


class ProductEdit(ProductMixin, generic.UpdateView):
    template_name = 'products/form.html'
    success_url = reverse_lazy('products:list')
    form_class = ProductForm


class ProductDelete(LoginRequiredMixin, ProductMixin, generic.DeleteView):
    template_name = 'delete.html'
    success_url = reverse_lazy('products:list')
