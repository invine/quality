from django.contrib import admin

from .models import Product, Size


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Size)
class SizeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
