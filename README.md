# InVine Quality Assurance App #

## Instruções para instalar e correr o projecto. ##

1. Instalar requisitos de sistema:
`$ sudo apt-get install python python-virtualenv`

2. Copiar aplicação:
`$ git clone https://bitbucket.org/invine/quality.git ~/quality-assurance/`

3. Mudar de directório
`$ cd ~/quality-assurance/`

4. Preparar requisitos da aplicação:
`$ ./bootstrap.sh`

Para correr a aplicação:
`$ source ~/quality-assurance/env/bin/activate && python ~/quality-assurance/quality/manage.py runserver 8000`

Comando para correr testes:
`$ python ~/quality-assurance/quality/manager.py harvest`

### Bibliotecas utilizadas
* [Django](http://www.djangoproject.com) v1.9.3
* [Lettuce](http://lettuce.it/) v0.2.21
* [Selenium](http://selenium-python.readthedocs.org/) v2.52.0

### Dados de login

**username**: qualityuser

**password**: qasupertester

### Algum problema na instalação?

Se existir algum problema na instalação ou a correr a aplicação contacta danielc@invine.com .

## Especificação da aplicação

* Página inicial deve ter um form com *username* e *password* e uma listagem de produtos.
* O user deve ser capaz de introduzir *username* e *password*.
* Caso *username* e *password* sejam válidos, deve manter-se a listagem de produtos, agora com as opções apagar, editar e adicionar produto.
	* Clicar na opção editar deve encaminhar o user para uma página de edição com os campos Descrição, Tamanhos e Nome de Produto.
	* Os campos *descrição* e *nome* devem ser editáveis.
	* No campo *tamanhos* deve ser possível selecionar um ou mais tamanhos.
	* Os campos *nome* e *tamanhos* são obrigatórios.
	* O campo *descrição* é facultativo.
	* Depois de gravar as alterações, deve voltar à página de Listagem de produtos.
	* Clicar na opção apagar, deve apagar o produto e este desaparece da listagem.
	* Deve aparecer uma confirmação com as opções apagar e cancelar.
	* Clicar em adicionar produto deve mostrar uma página semelhante à de editar mas com os campos por preencher. Depois de gravar volta à página de listagem.
	* Depois de editar ou adicionar produto, deve voltar à página de listagem de produtos com os seguintes campos visíveis:
		* *Nome* 
	    * *Tamanhos*
    	* *Descrição*
* Caso a autenticação não seja válida deve manter-se a lista de produtos sem ações.
* O utilizador deve ser capaz de introduzir novamente *username* e *password*.

----
Obrigado por te candidatares à [InVine](http://www.invine.com) e boa sorte!